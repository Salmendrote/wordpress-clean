<?php

// BEGIN iThemes Security - No modifiques ni borres esta línea
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Desactivar editor de archivos - Seguridad > Ajustes > Ajustes WordPress > Editor de archivos
// END iThemes Security - No modifiques ni borres esta línea

/**
 * Nuevo directorio para WP-CONTENT
 */
$new_wp_content = 'content-web-clean';
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/' . $new_wp_content );
define( 'WP_CONTENT_URL', (isset($_SERVER['HTTPS']) ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'] . '/wordpress-clean/' . $new_wp_content );

/**
 * Nuevo directorio para UPLOADS
 */
define( 'UPLOADS', $new_wp_content . '/media' );

/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress_clean');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'arar-00');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Ou+4R(1|ey0k,6Is#V11.`B{h30SbBt*v3HK1.ISvR*$XI|}|C4zsY0w?ZZoAp~h');
define('SECURE_AUTH_KEY', 'p_&s0]iN PeGpEzdCpmD)%:#]^:9 n;PmUBpg>$!C3N(bS?XJ[G@`rC)KsOa0 L ');
define('LOGGED_IN_KEY', '7lKw!p%$]E;%micSJU~,@+qAWy A>mbt0js2g%~k&_N^QGih4j)t%DfbeA+mdM?b');
define('NONCE_KEY', 'leeO,als+~4Q1MQ]EDX*?#>+w)4V,aa2=1[j${yke7l#kx&(VZ[hY88^X*wWrcz2');
define('AUTH_SALT', 'ub<!V/~y};h6 em,Qcq-HE_qC;QcQ6htn(;6P~w$hf?s_G`W53T+H=>O5^Bt*{5U');
define('SECURE_AUTH_SALT', ')homePkC;|hRB]yQ2BW@6|E~falVOU8bo_f4f8x$q,fG6egTh^=,PGgGq65PYS`r');
define('LOGGED_IN_SALT', 'x2av{`pr3;6B[KtUo5}(lssaWb-g*HGM6{CVeI2hZp,G--|+eKxk*3$|VqU2Ixi>');
define('NONCE_SALT', 'S%w~s6kZ=-;C4mmq#.)};Vo`4J/u?prgGz*1e9}.9YRa)8@EjyeZ]i0<A0ZNe&wm');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_clean_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Desactivar TODAS las actualizaciones automáticas en WordPress
 */
define( 'AUTOMATIC_UPDATER_DISABLED', false );

/**
 * Desactivar las actualizaciones automáticas del núcleo de WordPress
 */
//define( 'WP_AUTO_UPDATE_CORE', false );
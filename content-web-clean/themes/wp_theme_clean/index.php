<?php

/**
 * Vars (general)
 */
include_once realpath(__DIR__) . '/php/vars-pages.php';


if ( is_front_page() ) {

    // Plantilla
    $template = 'pages/home';

    // Contenido
    $page             = Timber::get_post();
    $context['page']  = $page;
    $context['title'] = $page->title;   

    $context['class_html']  = 'html__homepage';
    $context['class_body']  = 'body__homepage';

    // $context['js_vendor']  = [ '' ];
    // $context['js_extra']  = [ '' ];

}else if ( is_home() ) {

    // Plantilla
    $template = 'blog';

    // Contenido
    if ( have_posts() ) {
        $context['has_posts']  = TRUE;
        $context['posts']      = Timber::get_posts();
        $context['pagination'] = Timber::get_pagination();
    }

} else if ( is_page() ) {

    // Plantilla
    $template = 'page';

    // Contenido
    $page             = Timber::get_post();
    $context['page']  = $page;
    $context['title'] = $page->title;

} else if ( is_archive() ) {

    // Plantilla
    $template = 'archive';

    // Contenido
    $context['title'] = get_the_archive_title();
    if ( have_posts() ) {
        $context['has_posts']  = TRUE;
        $context['posts']      = Timber::get_posts();
        $context['pagination'] = Timber::get_pagination();
    }

} else if ( is_search() ) {

    // Plantilla
    $template = 'search';

    // Contenido
    $context['title'] = esc_html( get_search_query() );
    if ( have_posts() ) {
        $context['has_posts']  = TRUE;
        $context['posts']      = Timber::get_posts();
        $context['pagination'] = Timber::get_pagination();
    }

} else if ( is_single() ) {

    // Plantilla
    $template = 'single';

    // Contenido
    $post              = Timber::get_post();
    $context['post']   = $post;
    $context['author'] = array(
        'avatar'      => get_avatar( get_the_author_meta( 'user_email' ), 100 ),
        'name'        => get_the_author(),
        'description' => get_the_author_meta( 'description' ),
        'link'        => get_author_posts_url( get_the_author_meta( 'ID' ) ),
    );

} else {

    wp_redirect(home_url());

}




Timber::render( $template . '.twig', $context );

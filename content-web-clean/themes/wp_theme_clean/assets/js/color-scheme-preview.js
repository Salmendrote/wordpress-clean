(function($) {
    var style = $('#wp-theme-clean-color-scheme-css'),
        api = wp.customize;

    if (!style.length) {
        style = $('head').append('<style type="text/css" id="wp-theme-clean-color-scheme-css" />').find('#wp-theme-clean-color-scheme-css');
    }
    // Color Scheme CSS.
    api.bind('preview-ready', function() {
        api.preview.bind('update-color-scheme-css', function(css) {
            style.html(css);
        });
    });
})(jQuery);
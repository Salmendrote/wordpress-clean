<?php

/**
 * Timber is required
 */
if ( ! class_exists( 'Timber' ) ) {
  add_action( 'admin_notices', function() {
    echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
  return;
}


/**
 * View templates
 */
Timber::$dirname = ['views'];

/**
 * Tamaño máximo de archivos
 */
@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );

/**
 * Extra support
 */
function add_extra_support_to_theme() {
  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails
   */
  // add_theme_support( 'post-thumbnails' );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  // add_theme_support( 'html5', [
  //   'search-form',
  //   'comment-form',
  //   'comment-list',
  //   'gallery',
  //   'caption',
  // ] );

  /*
   * Enable support for Post Formats.
   * See https://developer.wordpress.org/themes/functionality/post-formats/
   */
  // add_theme_support( 'post-formats', [
  //   'aside',
  //   'image',
  //   'video',
  //   'quote',
  //   'link',
  //   'gallery',
  //   'status',
  //   'audio',
  //   'chat'
  // ] );

  /*
   * Make theme available for translation.
   * Translations can be filed in the /languages/ directory.
   * If you're building a theme based on underscore, use a find and replace
   * to change 'underscore' to the name of your theme in all the template files.
   */
  // load_theme_textdomain( THEME_NAME, get_template_directory() . '/languages' );

  // Menús del tema
  register_nav_menus([
    'main'   => esc_html__( 'Menú principal', THEME_NAME ),
  //   'footer' => esc_html__( 'Menú pie de página', THEME_NAME ),
  //   // 'social' => esc_html__( 'Menú redes sociales', THEME_NAME )
  ]);

  // Woocommerce support
  // add_theme_support( 'woocommerce' );

  // Sidebar
  // register_sidebar([
  //   'name'          => 'Sidebar BLOG',
  //   'id'            => 'sidebar-blog',
  //   'before_widget' => '<div class="w-wrapper">',
  //   'after_widget'  => '</div>',
  //   'before_title'  => '<h4 class="w-title">',
  //   'after_title'   => '</h4>'
  // ]);

  // Widgets para el FOOTER
  // for ($i=1; $i <= 5; $i++) {
  //   register_sidebar([
  //     'name'          => 'Widget Footer ' . $i,
  //     'id'            => 'footer-' . $i,
  //     'before_widget' => '<div class="w-wrapper  w-footer">',
  //     'after_widget'  => '</div>',
  //     'before_title'  => '<h5 class="w-title">',
  //     'after_title'   => '</h5>'
  //   ]);
  // }
}
add_action('after_setup_theme', 'add_extra_support_to_theme');


/**
 * Vendors JS & CSS
 */
function wp_custom_load_scripts() {
    // No cargar jQuery por defecto de Wordpress
    if ( !is_admin() ) {
      wp_deregister_script('jquery');
      wp_deregister_script('wp-embed');
      // wp_register_script('jquery', DIR_JS_VENDOR . 'jquery.min.js', FALSE, '3.3.1', TRUE);
      // wp_enqueue_script('jquery');
    }

    // Estilo principal
    wp_enqueue_style( 'wp_custom_style_css', THEME_URL . 'css/style.min.css?v=' . MOD_DATE );

    // Estilos WPML
    // wp_dequeue_style( 'wpml-legacy-horizontal-list-0' );
}
add_action( 'wp_enqueue_scripts', 'wp_custom_load_scripts' );


/**
 * General & theme options
 */
if ( function_exists('acf_add_options_page') ) {
  // Theme
  $tema = acf_add_options_page([
    'page_title' => 'Configuración del Tema',
    'menu_title' => 'Configuración del Tema',
    'menu_slug'  => 'conf_theme',
    'position'   => 2,
    'icon_url'   => 'dashicons-admin-settings',
    'capability' => 'edit_posts',
    'redirect'   => true
  ]);

  // General options
  acf_add_options_sub_page([
    'post_id'     => 'options',
    'page_title'  => 'Opciones generales',
    'menu_title'  => 'General',
    'parent_slug' => $tema['menu_slug'],
  ]);
}

/**
 * Cambiar remitente emails
 */
// add_filter('wp_mail_from', 'new_mail_from');
// add_filter('wp_mail_from_name', 'new_mail_from_name');
// function new_mail_from($old) {
//   return 'info@web.es';
// }
// function new_mail_from_name($old){
//   return 'clean';
// }

/**
 * Google Maps API
 */
// function my_acf_init() {
//   acf_update_setting('google_api_key', API_KEY_GOOGLE_MAPS);
// }
// add_action('acf/init', 'my_acf_init');

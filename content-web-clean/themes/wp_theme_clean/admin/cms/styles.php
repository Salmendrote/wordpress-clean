<?php

/**
 * Custom login form
 */
function custom_login_logo() {
    // echo '<style type="text/css">
    //     .login h1 a { background:url("'. home_url() .'/content-web-clean/media/2017/11/logo_clean.svg") left center no-repeat, url("'. home_url() .'/content-web-clean/media/2017/12/logo_clean_texto.svg") right bottom no-repeat !important; width: 255px; height: 120px; margin: 0 auto 20px; cursor: pointer;}
    // </style>';
 }
add_action('login_head', 'custom_login_logo');

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
 
function my_login_logo_url_title() {
    return get_bloginfo( 'name' );
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/*
 * Modificar estilos admin
 */
add_action('admin_head', 'my_custom_styles');

function my_custom_styles() {
    ?> 
    <style>
      #types-information-table, #aam-acceess-manager{
        display: none !important;
      }
    </style> 
    <?php
}


/**
 * Quitar la barra de admin de WordPress
 */
//add_filter( 'show_admin_bar', '__return_false' );


/**
 * Modificar menú de administración de wordpress
 */
add_action( 'admin_menu', 'apk_eliminar_admin_menu_links' );
 
function apk_eliminar_admin_menu_links() {
  remove_menu_page('edit.php'); // Removemos el ítem Entradas
  remove_menu_page('edit-comments.php'); // Removemos el ítem comentarios
}

/**
 * Eliminar metaboxes de las entradas
 */
function my_remove_meta_boxes() {
  remove_meta_box('icl_div_config', 'proyecto', 'normal');
  remove_meta_box('commentstatusdiv', 'proyecto', 'normal');
  remove_meta_box('commentsdiv', 'proyecto', 'normal');
  remove_meta_box('revisionsdiv', 'proyecto', 'normal');
  remove_meta_box('authordiv', 'proyecto', 'normal');
  remove_meta_box('icl_div_config', 'page', 'normal');
  remove_meta_box('commentstatusdiv', 'page', 'normal');
  remove_meta_box('commentsdiv', 'page', 'normal');
  remove_meta_box('revisionsdiv', 'page', 'normal');
  remove_meta_box('authordiv', 'page', 'normal');
}
add_action( 'admin_menu', 'my_remove_meta_boxes' );


/**
 * Eliminar opciones de la barra del administrador
 */
function change_toolbar($wp_toolbar) {  
  $user = wp_get_current_user(); //Obtenemos los datos del usuario actual

  $wp_toolbar->remove_node('comments');
  $wp_toolbar->remove_node('new-post');  
}   
add_action('admin_bar_menu', 'change_toolbar', 999); 
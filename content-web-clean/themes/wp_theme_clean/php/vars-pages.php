<?php

if ( ! class_exists( 'Timber' ) ) {
    echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
    return;
}


/**
 * Context
 */
$context = Timber::get_context();


/**
 * Directories
 */
$dir_pages    = 'pages/';
// $dir_products = 'products/';
// $dir_blog     = 'blog/';

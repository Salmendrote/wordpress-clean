<?php

// Generales
define( 'ENV', 					getenv('ENV') );
define( 'BASE_URL',				getenv('BASE_URL') );
define( 'DIR_TEMPLATE_THEME',	getenv('DIR_TEMPLATE_THEME') );
define( 'THEME_URL', 			BASE_URL . DIR_TEMPLATE_THEME );
define( 'AJAX_URL',				THEME_URL . 'ajax.php' );
define( 'SLASH',				'/' );
define( 'DIR_VENDOR',			'vendor' );

// VISTAS / PLANTILLAS
define( 'DIR_VIEWS',		'views' . SLASH );
define( 'DIR_VIEWS_PAGES',	'pages' . SLASH );
define( 'DIR_VIEWS_ERRORS', 'errors' . SLASH );

// PHP
define( 'DIR_PHP', 			'php' . SLASH );
define( 'DIR_PHP_VENDOR',	DIR_PHP . DIR_VENDOR . SLASH );

// Images
define( 'DIR_IMAGES', 			'images' . SLASH );
define( 'DIR_IMAGES_ABS', 		THEME_URL . DIR_IMAGES );
define( 'DIR_IMAGES_HOME', 		DIR_IMAGES_ABS . 'home/' );

// Ficheros
define( 'DIR_FILES', 		'files' . SLASH );

// Vídeos
define( 'DIR_VIDEOS', 		DIR_FILES . SLASH . 'videos' . SLASH );
define( 'DIR_VIDEOS_ABS', 	THEME_URL . DIR_VIDEOS );

// Estilos CSS
define( 'DIR_CSS', 			'css' . SLASH );
define( 'DIR_CSS_ABS', 		THEME_URL . DIR_CSS );
define( 'DIR_CSS_VENDOR', 	DIR_CSS_ABS . DIR_VENDOR . SLASH );

// Javascript
define( 'DIR_JS', 			'js' . SLASH );
define( 'DIR_JS_ABS', 		THEME_URL . DIR_JS );
define( 'DIR_JS_VENDOR', 	DIR_JS_ABS . DIR_VENDOR . SLASH );

// ID's páginas
define( 'ID_PAGE_HOME', 		            2 );

// ID's categorías
define( 'ID_CAT_SIN_CATEGORIA', 1 );


// Correos
define( 'MAIL_HOST',			'' );
define( 'MAIL_PORT',			'25' );
define( 'MAIL_WEB_USERNAME',	'' );
define( 'MAIL_WEB_PASSWORD',	'' );


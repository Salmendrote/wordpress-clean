<?php

class WP_Theme_Clean_Color_Scheme {

    public $is_custom = FALSE;
    public $options   = array(
        'text_color',
        'link_color',
        'button_background_color',
        'button_hover_background_color',
        'section_dark_background_color',
        'footer_background_color',
        'highlight_color',
    );

    public function __construct() {
        add_action( 'customize_register',                       array( $this, 'customizer_register' ) );
        add_action( 'customize_controls_enqueue_scripts',       array( $this, 'customize_js' ) );
        add_action( 'customize_controls_print_footer_scripts',  array( $this, 'color_scheme_template' ) );
        add_action( 'customize_controls_enqueue_scripts',                   array( $this, 'customize_preview_js' ) );
        add_action( 'wp_enqueue_scripts',                       array( $this, 'output_css' ) );
    }

    public function get_color_schemes() {
        return array(
            'default' => array(
                'label'  => __( 'Por defecto', THEME_NAME ),
                'colors' => array(
                    '#000000',
                    '#41535d',
                    '#e67e22',
                    '#c35b00',
                    '#2c383f',
                    '#222b30',
                    '#e67e22',
                ),
            ),
            'orange'    => array(
                'label'  => __( 'Oscuro', THEME_NAME ),
                'colors' => array(
                    '#777777',
                    '#dd8500',
                    '#1d5d8e',
                    '#00508e',
                    '#aa6600',
                    '#9d5f00',
                    '#dd8500',
                ),
            ),
            // Other color schemes
        );
    }

    // Opciones de APARIENCIA
    public function customizer_register( WP_Customize_Manager $wp_customize ) {

        // Nueva pestaña "Colores" para elegir el esquema de colores
        $wp_customize->add_section( 'colors', array(
            'title' => __( 'Colores', THEME_NAME ),
        ) );

        // Select para elegir el esquema de colores
        $wp_customize->add_setting( 'color_scheme', array(
            'sanitize_callback' => 'sanitize_color_scheme',
            'default'           => 'default',
            'transport'         => 'postMessage',
        ) );
        $color_schemes = $this->get_color_schemes();
        $choices       = array();
        foreach ( $color_schemes as $color_scheme => $value ) {
            $choices[$color_scheme] = $value['label'];
        }
        $wp_customize->add_control( 'color_scheme', array(
            'label'   => __( 'Esquema de colores', THEME_NAME ),
            'section' => 'colors',
            'type'    => 'select',
            'choices' => $choices,
        ) );

        // Opciones para seleccionar los colores
        $options = array(
            'text_color'                    => __( 'Color de texto', THEME_NAME ),
            'link_color'                    => __( 'Color de enlaces', THEME_NAME ),
            'button_background_color'       => __( 'Button background color', THEME_NAME ),
            'button_hover_background_color' => __( 'Button hover background color', THEME_NAME ),
            'section_dark_background_color' => __( 'Section dark background color', THEME_NAME ),
            'footer_background_color'       => __( 'Footer background color', THEME_NAME ),
            'highlight_color'               => __( 'Hightlight color', THEME_NAME ),
        );
        foreach ( $options as $key => $label ) {
            $wp_customize->add_setting( $key, array(
                'sanitize_callback' => 'sanitize_hex_color',
                'transport'         => 'postMessage',
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $key, array(
                'label'   => $label,
                'section' => 'colors',
            ) ) );
        }

    }

    public function get_color_scheme() {
        $color_schemes = $this->get_color_schemes();
        $color_scheme  = get_theme_mod( 'color_scheme' );
        $color_scheme  = isset( $color_schemes[$color_scheme] ) ? $color_scheme : 'default';

        if ( 'default' != $color_scheme ) {
            $this->is_custom = true;
        }

        $colors = array_map( 'strtolower', $color_schemes[$color_scheme]['colors'] );

        foreach ( $this->options as $k => $option ) {
            $color = get_theme_mod( $option );
            if ( $color && strtolower( $color ) != $colors[$k] ) {
                $colors[$k]      = $color;
                $this->is_custom = true;
            }
        }
        return $colors;
    }

    public function output_css() {
        $colors = $this->get_color_scheme();
        if ( $this->is_custom ) {
            wp_add_inline_style( 'wp_theme_clean_style_css', $this->get_css( $colors ) );
        }
    }

    public function get_css( $colors ) {
        $css = '
            h1, h1 a,
            h2, h2 a,
            h3, h3 a,
            a {
                color: %1$s;
            }
            blockquote {
                border-left-color: %2$s;
            }
            code {
                color: %6$s;
            }
            button,
            input[type="submit"],
            .button {
                background: %2$s;
            }
            .section h2 span,
            .number {
                color: %6$s;
            }
            .section--dark,
            .services-1 {
                background-color: %4$s;
            }
            .footer {
                background: %5$s;
            }';
        // More CSS
        return vsprintf( $css, $colors );
    }

    public function color_scheme_template() {
        $colors = array(
            'text_color'                    => '{{ data.text_color }}',
            'link_color'                    => '{{ data.link_color }}',
            'button_background_color'       => '{{ data.button_background_color }}',
            'button_hover_background_color' => '{{ data.button_hover_background_color }}',
            'section_dark_background_color' => '{{ data.section_dark_background_color }}',
            'footer_background_color'       => '{{ data.footer_background_color }}',
            'highlight_color'               => '{{ data.highlight_color }}',
        );
        ?>
        <script type="text/html" id="tmpl-wp-theme-clean-color-scheme">
            <?php echo $this->get_css( $colors ); ?>
        </script>
        <?php
    }

    public function customize_js() {
        wp_enqueue_script( 'wp-theme-clean-color-scheme', get_template_directory_uri() . '/js/color-scheme.min.js', array( 'customize-controls', 'iris', 'underscore', 'wp-util' ), '', true );
        wp_localize_script( 'wp-theme-clean-color-scheme', 'WpThemeCleanColorScheme', $this->get_color_schemes() );
    }

    public function customize_preview_js() {
        wp_enqueue_script( 'wp-theme-clean-color-scheme-preview', get_template_directory_uri() . '/js/color-scheme-preview.min.js', array( 'customize-preview' ), '', true );
    }


}
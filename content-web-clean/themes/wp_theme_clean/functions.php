<?php

//COMPRUEBA SI ES UN ROBOT
function is_bot()
{
    $bots = array(
            'Googlebot', 'Baiduspider', 'ia_archiver',
            'R6_FeedFetcher', 'NetcraftSurveyAgent', 'Sogou web spider',
            'bingbot', 'Yahoo! Slurp', 'facebookexternalhit', 'PrintfulBot',
            'msnbot', 'Twitterbot', 'UnwindFetchor',
            'urlresolver', 'Butterfly', 'TweetmemeBot'
    );

    foreach($bots as $b) {
        if( stripos( $_SERVER['HTTP_USER_AGENT'], $b ) !== false ) return true;
    }

    return false;
}

if ( !is_bot() ) {
    include ( realpath(__DIR__) . '/php/vendor/modernizr/php/modernizr-server.php');
    foreach($modernizr as $feature=>$value) {
      if($feature == 'flexbox' && $value <> 1){
        wp_redirect( 'https://myaccount.google.com/not-supported?hl=es_ES&ref=/settings/storage' );
        exit;
      }
    }
}

if ( ! class_exists( 'Timber' ) ) {
  add_action( 'admin_notices', function() {
      echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
  return;
}

// Dirs
$dir_admin = realpath(__DIR__) . '/' . 'admin/';
$dir_cms   = $dir_admin . 'cms/';

// PHP vendor libraries
include_once realpath(__DIR__) . '/php/vendor/autoload.php';

// Initiate "phpdotenv" for using ".env" files
$dotenv = new Dotenv\Dotenv( realpath(__DIR__) );
$dotenv->load();

// Constants
include_once realpath(__DIR__) . '/php/constants.php';

// CMS
include_once $dir_cms . 'styles.php';

// Theme
include_once $dir_admin . 'theme.php';




/**
 * Agregar cabeceras de seguridad
 */
if (ENV == 'produccion') {
  function add_header_security() {
    // Tiene como función para aquellos navegadores que soportan esta condición, no cargar las hojas de estilos y scripts cuyo Mime-type no sea el adecuado
    header('X-Content-Type-Options: nosniff');
    // Tienen como función prevenir que la página pueda ser abierta en un frame o iframe. De esta forma se pueden evitar ataques clickjacking sobre tu web.
    header('X-Frame-Options: SAMEORIGIN');
    // Activar para aquellos navegadores que disponen de la funcionalidad el filtro XSS. Capa de seguridad adicional que bloquea ataques XSS. Internet Explorer lo implementa desde su versión 8.
    header('X-XSS-Protection: 1;mode=block');
  }
  add_action('send_headers', 'add_header_security');
}

class StarterSite extends TimberSite {

  function __construct() {
    add_filter( 'timber_context', [$this, 'add_to_context'] );
    //add_filter( 'get_twig', [$this, 'add_to_twig'] );
    parent::__construct();
  }

  function add_to_context( $context ) {
    // Menú Wordpress
    $context['menu_main']   = new TimberMenu('main');
    $context['menu_footer'] = new TimberMenu('footer');

    // Opciones generales del TEMA
    $context['options'] = get_fields( 'options' );

    // Sitio
    $context['site'] = $this;

    return $context;
  }

  // function add_to_twig( $twig ) {
  //   $twig->addExtension( new Twig_Extension_StringLoader() );
  //   $twig->addFilter( new Twig_SimpleFilter( 'cat_classes', function($id){
  //     $product    = Timber::get_post($id);
  //     $categories = $product->terms;
  //     $total      = count($categories);
  //     $count      = 0;
  //     $classes    = '';
  //     foreach ($categories as $cat) {
  //       $classes .= 'js-cat-' . $cat->slug;
  //       if ( $count < $total ) {
  //         $classes .= '  ';
  //       }
  //       $count++;
  //     }
  //     return $classes;
  //   } ) );
  //   return $twig;
  // }

}

new StarterSite();